CREATE DATABASE  IF NOT EXISTS `magic` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `magic`;
-- MySQL dump 10.13  Distrib 5.6.24, for Win64 (x86_64)
--
-- Host: localhost    Database: magic
-- ------------------------------------------------------
-- Server version	5.6.26-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `card`
--

DROP TABLE IF EXISTS `card`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `card` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(30) NOT NULL,
  `color` varchar(20) DEFAULT NULL,
  `rarity` varchar(20) DEFAULT NULL,
  `description` varchar(500) DEFAULT NULL,
  `releaselogo` varchar(20) DEFAULT NULL,
  `island` varchar(2) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `card`
--

LOCK TABLES `card` WRITE;
/*!40000 ALTER TABLE `card` DISABLE KEYS */;
INSERT INTO `card` VALUES (1,'Awesome Card1','W','rare','This card is so awesome and wonderful and blah blah blah','zendikar','0'),(2,'Awesome Card2','W','rare','This card is so awesome and wonderful and blah blah blah','zendikar','0'),(3,'Awesome Card3','W','rare','This card is so awesome and wonderful and blah blah blah','zendikar','0'),(4,'Awesome Card4','W','rare','This card is so awesome and wonderful and blah blah blah','zendikar','0'),(5,'Awesome Card5','W','rare','This card is so awesome and wonderful and blah blah blah','zendikar','0'),(6,'Awesome Card6','W','rare','This card is so awesome and wonderful and blah blah blah','zendikar','0'),(7,'Awesome Card7','W','rare','This card is so awesome and wonderful and blah blah blah','zendikar','0'),(8,'Awesome Card8','W','rare','This card is so awesome and wonderful and blah blah blah','zendikar','0'),(9,'Awesome Card9','W','rare','This card is so awesome and wonderful and blah blah blah','zendikar','0'),(10,'Awesome Card10','W','rare','This card is so awesome and wonderful and blah blah blah','zendikar','0');
/*!40000 ALTER TABLE `card` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `creature`
--

DROP TABLE IF EXISTS `creature`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `creature` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `power` int(11) DEFAULT NULL,
  `toughness` int(11) DEFAULT NULL,
  `NONLAND_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `cardid_UNIQUE` (`id`),
  KEY `fk_CREATURE_NONLAND1_idx` (`NONLAND_id`),
  CONSTRAINT `fk_CREATURE_NONLAND1` FOREIGN KEY (`NONLAND_id`) REFERENCES `nonland` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `creature`
--

LOCK TABLES `creature` WRITE;
/*!40000 ALTER TABLE `creature` DISABLE KEYS */;
INSERT INTO `creature` VALUES (1,3,4,2),(2,3,4,4),(3,3,4,6),(4,3,4,8),(5,3,4,10);
/*!40000 ALTER TABLE `creature` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dcontainscard`
--

DROP TABLE IF EXISTS `dcontainscard`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dcontainscard` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `quantity` int(11) NOT NULL DEFAULT '0',
  `DECK_id` int(11) NOT NULL,
  `CARD_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  KEY `fk_DCONTAINSCARD_DECK1_idx` (`DECK_id`),
  KEY `fk_DCONTAINSCARD_CARD1_idx` (`CARD_id`),
  CONSTRAINT `fk_DCONTAINSCARD_CARD1` FOREIGN KEY (`CARD_id`) REFERENCES `card` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_DCONTAINSCARD_DECK1` FOREIGN KEY (`DECK_id`) REFERENCES `deck` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dcontainscard`
--

LOCK TABLES `dcontainscard` WRITE;
/*!40000 ALTER TABLE `dcontainscard` DISABLE KEYS */;
INSERT INTO `dcontainscard` VALUES (1,2,1,1),(2,2,1,2),(3,2,1,3),(4,2,1,4),(5,2,1,5),(6,2,1,6),(7,2,1,7),(8,2,1,8);
/*!40000 ALTER TABLE `dcontainscard` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `deck`
--

DROP TABLE IF EXISTS `deck`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `deck` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(20) DEFAULT NULL,
  `username` varchar(20) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  KEY `fk_DECK_USER1_idx` (`username`),
  CONSTRAINT `fk_DECK_USER1` FOREIGN KEY (`username`) REFERENCES `user` (`username`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `deck`
--

LOCK TABLES `deck` WRITE;
/*!40000 ALTER TABLE `deck` DISABLE KEYS */;
INSERT INTO `deck` VALUES (1,'mydeck','genevieve'),(2,'mydeck1','genevieve'),(3,'mydeck2','genevieve'),(4,'mydeck3','genevieve'),(5,'mydeck4','genevieve'),(6,'mydeck5','genevieve'),(7,'mydeck6','genevieve'),(8,'yourdeck','shengjie'),(9,'yourdeck1','shengjie'),(10,'yourdeck2','shengjie'),(11,'yourdeck3','shengjie');
/*!40000 ALTER TABLE `deck` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `icontainscard`
--

DROP TABLE IF EXISTS `icontainscard`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `icontainscard` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `quantity` int(11) NOT NULL DEFAULT '0',
  `CARD_id` int(11) NOT NULL,
  `INVENTORY_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_ICONTAINSCARD_CARD1_idx` (`CARD_id`),
  KEY `fk_ICONTAINSCARD_INVENTORY1_idx` (`INVENTORY_id`),
  CONSTRAINT `fk_ICONTAINSCARD_CARD1` FOREIGN KEY (`CARD_id`) REFERENCES `card` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_ICONTAINSCARD_INVENTORY1` FOREIGN KEY (`INVENTORY_id`) REFERENCES `inventory` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `icontainscard`
--

LOCK TABLES `icontainscard` WRITE;
/*!40000 ALTER TABLE `icontainscard` DISABLE KEYS */;
INSERT INTO `icontainscard` VALUES (1,2,1,1),(2,2,2,1),(3,2,3,1),(4,2,4,1),(5,2,5,1),(6,2,6,1),(7,2,7,1),(8,2,8,1);
/*!40000 ALTER TABLE `icontainscard` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `icontainsdeck`
--

DROP TABLE IF EXISTS `icontainsdeck`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `icontainsdeck` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `INVENTORY_id` int(11) NOT NULL,
  `DECK_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  KEY `fk_ICONTAINSDECK_INVENTORY1_idx` (`INVENTORY_id`),
  KEY `fk_ICONTAINSDECK_DECK1_idx` (`DECK_id`),
  CONSTRAINT `fk_ICONTAINSDECK_DECK1` FOREIGN KEY (`DECK_id`) REFERENCES `deck` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_ICONTAINSDECK_INVENTORY1` FOREIGN KEY (`INVENTORY_id`) REFERENCES `inventory` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `icontainsdeck`
--

LOCK TABLES `icontainsdeck` WRITE;
/*!40000 ALTER TABLE `icontainsdeck` DISABLE KEYS */;
INSERT INTO `icontainsdeck` VALUES (1,1,1),(2,1,2),(3,1,3),(4,1,4),(5,1,5),(6,1,6),(7,1,7);
/*!40000 ALTER TABLE `icontainsdeck` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `inventory`
--

DROP TABLE IF EXISTS `inventory`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `inventory` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(20) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  KEY `fk_INVENTORY_USER1_idx` (`username`),
  CONSTRAINT `fk_INVENTORY_USER1` FOREIGN KEY (`username`) REFERENCES `user` (`username`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `inventory`
--

LOCK TABLES `inventory` WRITE;
/*!40000 ALTER TABLE `inventory` DISABLE KEYS */;
INSERT INTO `inventory` VALUES (1,'genevieve'),(2,'genevieve'),(3,'genevieve'),(6,'leixiong'),(5,'peng'),(4,'shengjie'),(7,'suwara'),(8,'suwara1'),(9,'suwara2'),(10,'suwara3');
/*!40000 ALTER TABLE `inventory` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `nonland`
--

DROP TABLE IF EXISTS `nonland`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `nonland` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `manacost` varchar(30) DEFAULT NULL,
  `isplaneswalker` varchar(2) NOT NULL,
  `iscreature` varchar(2) NOT NULL,
  `CARD_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `cardid_UNIQUE` (`id`),
  KEY `fk_NONLAND_CARD1_idx` (`CARD_id`),
  CONSTRAINT `fk_NONLAND_CARD1` FOREIGN KEY (`CARD_id`) REFERENCES `card` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `nonland`
--

LOCK TABLES `nonland` WRITE;
/*!40000 ALTER TABLE `nonland` DISABLE KEYS */;
INSERT INTO `nonland` VALUES (1,'3W,4B','1','0',1),(2,'4B,2G','0','1',2),(3,'4B,2G','1','0',3),(4,'4B,2G','0','1',4),(5,'4B,2G','1','0',5),(6,'4B,2G','0','1',6),(7,'4B,2G','1','0',7),(8,'5A','0','1',8),(9,'5A','1','0',9),(10,'5A','0','1',10);
/*!40000 ALTER TABLE `nonland` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `planeswalker`
--

DROP TABLE IF EXISTS `planeswalker`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `planeswalker` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `health` int(11) DEFAULT NULL,
  `NONLAND_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `cardid_UNIQUE` (`id`),
  KEY `fk_PLANESWALKER_NONLAND1_idx` (`NONLAND_id`),
  CONSTRAINT `fk_PLANESWALKER_NONLAND1` FOREIGN KEY (`NONLAND_id`) REFERENCES `nonland` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `planeswalker`
--

LOCK TABLES `planeswalker` WRITE;
/*!40000 ALTER TABLE `planeswalker` DISABLE KEYS */;
INSERT INTO `planeswalker` VALUES (1,4,1),(2,4,3),(3,3,5),(4,4,7),(5,4,9);
/*!40000 ALTER TABLE `planeswalker` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `username` varchar(20) NOT NULL,
  `password` varchar(20) NOT NULL,
  `email` varchar(30) NOT NULL,
  PRIMARY KEY (`username`),
  UNIQUE KEY `username_UNIQUE` (`username`),
  UNIQUE KEY `email_UNIQUE` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES ('12345','12345','12345'),('genevieve','12345678','smartartbug@gmail.com'),('hellomoto','hellomoto','hellomoto'),('leixiong','12345678','lxgxiong@gmail.com'),('peng','12345678','peng.zou@gmail.com'),('shengjie','12345678','shengjie999@gmail.com'),('suwara','12345678','suwara@gmail.com'),('suwara1','12345678','suwara1@gmail.com'),('suwara2','12345678','suwara2@gmail.com'),('suwara3','12345678','suwara3@gmail.com');
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-11-17 15:37:56
