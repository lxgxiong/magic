<%@page import="com.dto.CardDTO"%>
<%@page import="com.service.ServiceUtil"%>
<%@page import="com.entity.Card"%>
<%@page import="com.dao.DaoUtil"%>
<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<script type="text/javascript" src="/magic/js/jquery-1.11.3.min.js"></script>
<script>
$(document).ready(function(){
	
	function hide(index, forms){
		for (var i = 1; i < forms.length; i++) {
			$(forms[i]).hide();
			forms[i].reset();
		}
		$(forms[index]).show();
	}
	
	$('#radioSet input').change(function(){
		var value = $('input[name=types]:checked', '#radioSet').val();
		var forms= $('form');
		if(value>3){
			hide(4,forms);
		} else{
			hide(value,forms);
		}
	});

	
});

</script>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Magic--Adding Cards</title>
</head>
<body>
<h1>Adding Cards</h1>
	<table>
		<tr>
			<td style="width: 100px"><a href="index.jsp">Homepage</a></td>
			<td style="width: 100px"><a href="search.jsp?keyword=&indentoryId=<%=Integer.valueOf(session.getAttribute("inventoryid").toString())%>">Inventory</a></td>
			<td style="width: 100px"><a href="decks.jsp">Decks</a></td>
			<td style="width: 100px"><a href="addcards.jsp">Add Cards</a></td>
			<td style="width: 100px"><a href="login.jsp">Login</a></td>
		</tr>
	</table>
	<br>
	<%
	
		out.print("Captain: " + session.getAttribute("username"));
	%>
	<br>
	<h2>SELECT TYPE OF CARD</h2>
	
	<br>
	<form id="radioSet">
		<input type="radio" id="r1" name="types" value="1" >Land
		<input type="radio" id="r2" name="types" value="2">Planeswalker
		<input type="radio" id="r3" name="types" value="3">Creature
		<input type="radio" id="r4" name="types" value="4" >Enchantment
		<input type="radio" id="r5" name="types" value="5">Instant
		<input type="radio" id="r6" name="types" value="6">Sorcery
	</form>
	
		<form id="form1" action="AddCardAction" method="POST" style="display:none;">
	
		<input id="typeValue" name="type" type="hidden" value="1">
		<br><label>Name<input name="name" type="text"></label><br>
		<br>
		<label>Description <input name="description" type="text"></label>
		<br>
		<br>
		<label>Color <input name="color" type="text"></label>
		<br>
		<br>
		<label>Rarity <input name="rarity" type="text"></label>
		<br>
		<br>
		<label>Release Logo <input name="releaselogo" type="text"></label>
		<br>
		<br>
		<label>Quantity <input name="quantity" type="text"></label>
		<br>
		
	<button type="submit">Add</button>
	<button type="reset">Reset</button>

		
	</form>
	
		<form id="form2" action="AddCardAction" method="POST" style="display:none;">
	
		<input id="typeValue" name="type" type="hidden" value="2">
		<br><label>Name<input name="name" type="text"></label><br>
		<br>
		<label>Description <input name="description" type="text"></label>
		<br>
		<br>
		<label>Color <input name="color" type="text"></label>
		<br>
		<br>
		<label>Rarity <input name="rarity" type="text"></label>
		<br>
		<br>
		<label>Release Logo <input name="releaselogo" type="text"></label>
		<br>
		<br>
		<label>Quantity <input name="quantity" type="text"></label>
		<br>
		<br>
		<label>Mana Cost <input name="manacost" type="text"></label>
		<br>
		<br>
		<label>Health <input name="health" type="text"></label>
		<br>
		
	<button type="submit">Add</button>
	<button type="reset">Reset</button>

		
	</form>
	
		<form id="form3" action="AddCardAction" method="POST" style="display:none;">
	
		<input id="typeValue" name="type" type="hidden" value="3">
		<br><label>Name<input name="name" type="text"></label><br>
		<br>
		<label>Description <input name="description" type="text"></label>
		<br>
		<br>
		<label>Color <input name="color" type="text"></label>
		<br>
		<br>
		<label>Rarity <input name="rarity" type="text"></label>
		<br>
		<br>
		<label>Release Logo <input name="releaselogo" type="text"></label>
		<br>
		<br>
		<label>Quantity <input name="quantity" type="text"></label>
		<br>
		<br>
		<label>Mana Cost <input name="manacost" type="text"></label>
		<br>
		<br>
		<label>Power <input name="power" type="text"></label>
		<br>
		<br>
		<label>Toughness <input name="toughness" type="text"></label>
		<br>
		
	<button type="submit">Add</button>
	<button type="reset">Reset</button>

		
	</form>
	
		<form id="form4" action="AddCardAction" method="POST" style="display:none;">
	
		<input id="typeValue" name="type" type="hidden" value="4">
		<br><label>Name<input name="name" type="text"></label><br>
		<br>
		<label>Description <input name="description" type="text"></label>
		<br>
		<br>
		<label>Color <input name="color" type="text"></label>
		<br>
		<br>
		<label>Rarity <input name="rarity" type="text"></label>
		<br>
		<br>
		<label>Release Logo <input name="releaselogo" type="text"></label>
		<br>
		<br>
		<label>Quantity <input name="quantity" type="text"></label>
		<br>
		<br>
		<label>Mana Cost <input name="manacost" type="text"></label>
		<br>
		
	<button type="submit">Add</button>
	<button type="reset">Reset</button>

		
	</form>

</body>
</html>