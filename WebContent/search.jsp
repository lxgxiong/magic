<%@page import="com.entity.Deck"%>
<%@page import="com.dto.CardDTO"%>
<%@page import="com.service.ServiceUtil"%>
<%@page import="com.entity.Card"%>
<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Magic--Cards of Inventory</title>
<script type="text/javascript" src="/magic/js/jquery-1.11.3.min.js"></script>
<script type="text/javascript">
	function addToDeck(cardId,hasQuantity){
		var deckId = $('#deckSelection'+cardId).find(':selected').val();
		var quantity = document.getElementById("addQuantity"+cardId).value;
		var quantityInDeck = 0;
		$.ajax({
		    type: "POST",
		    url: "SelectQuantityInDeck",
		    data: {'deckId':deckId,'cardId':cardId},
		    dataType: "html",
		    success: function(data, textStatus) {
		    	data = JSON.parse(data);
		    	quantityInDeck = data.quantity;
		    	
		    	var has = Number(quantity)+Number(quantityInDeck);
		    	if(has > Number(hasQuantity)){
		    		alert("You cheater, you don't even have so many cards!!!You already have "+quantityInDeck+ " in this deck.");
		    	}else{
					$.ajax({
					    type: "POST",
					    url: "AddToDeckByIdAction",
					    data: {'deckId':deckId,'cardId':cardId,'quantity':quantity},
					    dataType: "html",
					    success: function(data, textStatus) {
				            //window.location.href = "inventory.jsp";
					    }
					});
				}
		    }
		});
	}
</script>
</head>
<body>
	<h1>Cards in Inventory</h1>
	<table>
		<tr>
			<td style="width: 100px"><a href="index.jsp">Homepage</a></td>
			<td style="width: 100px"><a href="search.jsp?keyword=&indentoryId=<%=Integer.valueOf(session.getAttribute("inventoryid").toString())%>">Inventory</a></td>
			<td style="width: 100px"><a href="decks.jsp">Decks</a></td>
			<td style="width: 100px"><a href="addcards.jsp">Add Cards</a></td>
			<td style="width: 100px"><a href="login.jsp">Login</a></td>
		</tr>
	</table>
		<br>
	<%
		out.print("Captain: " + session.getAttribute("username"));
	%>
	<form action="search.jsp" method="post">
		<input type="hidden" value="" name="inventoryId">
		<input type="text" name="keyword">
		<button type="submit">Search</button>
	</form>
	<table border="1">


		<tr>
			<td>ID</td>
			<td>Name</td>
			<td>Description</td>
			<td>Color</td>
			<td>Rarity</td>
			<td>ReleaseLogo</td>
			<td>Mana Cost</td>
			<td>Power</td>
			<td>Toughness</td>
			<td>Health</td>
			<td>Quantity</td>
			<td>Delete</td>
			<td>Add to Deck</td>
		</tr>

		<%
			String keyword = request.getParameter("keyword");
			List<Deck> decks = ServiceUtil.getDeckService().listByUserName((String) session.getAttribute("username"));
			for (int i = 0; i < decks.size(); i++) {
				Deck deck = decks.get(i);
			}

			int id = Integer.valueOf(session.getAttribute("inventoryid").toString());
			List<CardDTO> cards = ServiceUtil.getCardService().searchInInventory(keyword, id);
			for (int i = 0; i < cards.size(); i++) {
				CardDTO card = cards.get(i);
		%>
		<tr>
			<td><%=card.getId()%></td>
			<td><%=card.getName()%></td>
			<td><%=card.getDescription()%></td>
			<td><%=card.getColor()%></td>
			<td><%=card.getRarity()%></td>
			<td><%=card.getReleaseLogo()%></td>
			<td><%=card.getManaCost() %></td>
			<td><%=card.getPower() %></td>
			<td><%=card.getToughness() %></td>
			<td><%=card.getHealth() %></td>
			<td><%=card.getQuantity()%></td>
			<td><form action="DeleteCardFromInvAction" method="post">
					<label>Quantity:<input name="quantity" type="text" size="5"></label>
					<input name="id" value=<%=id%> type="hidden"> <input
						name="cardid" value=<%=card.getId()%> type="hidden"> <input
						name="has" value=<%=card.getQuantity()%> type="hidden">
					<button type="submit">Delete</button>
				</form></td>
			<td><select id="deckSelection<%=card.getId() %>" name="deckid">
					<%
						for (int j = 0; j < decks.size(); j++) {
								Deck deck = decks.get(j);
					%>
					<option value="<%=deck.getId()%>"><%=deck.getName()%></option>
					<%
						}
					%>

			</select> <label>Quantity:<input type="text"
					id="addQuantity<%=card.getId()%>" value="1" size="3"></label>
				<button
					onclick="addToDeck(<%=card.getId()%>,<%=card.getQuantity()%>)">Add
					to Deck</button></td>

		</tr>
		<%
			}
		%>
	</table>

</body>
</html>