<%@page import="com.dto.CardDTO"%>
<%@page import="com.service.ServiceUtil"%>
<%@page import="com.entity.Card"%>
<%@page import="com.dao.DaoUtil"%>
<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Magic--Cards</title>
</head>
<body>
<h1>Cards in Deck</h1>
	<table>
		<tr>
			<td style="width:100px"><a href = "index.jsp">Homepage</a></td>
			<td style="width: 100px"><a href="search.jsp?keyword=&indentoryId=<%=Integer.valueOf(session.getAttribute("inventoryid").toString())%>">Inventory</a></td>
			<td style="width:100px"><a href = "decks.jsp">Decks</a></td>
			<td style="width: 100px"><a href="addcards.jsp">Add Cards</a></td>
			<td style="width:100px"><a href = "login.jsp">Login</a></td>
		</tr>
	</table>
		<br>
	<%
		out.print("Captain: " + session.getAttribute("username"));
	%>
	<table border="1">


		<tr>
			<td>ID</td>
			<td>Name</td>
			<td>Description</td>
			<td>Color</td>
			<td>Rarity</td>
			<td>ReleaseLogo</td>
			<td>Mana Cost</td>
			<td>Power</td>
			<td>Toughness</td>
			<td>Health</td>
			<td>Quantity</td>
		</tr>

		<%
		int id = Integer.valueOf(request.getParameter("id"));
			List<CardDTO> cards = ServiceUtil.getCardService().listByDeckId(id);
			for (int i = 0; i < cards.size(); i++) {
				CardDTO card = cards.get(i);
		%>
		<tr>
			<td><%=card.getId()%></td>
			<td><%=card.getName()%></td>
			<td><%=card.getDescription() %></td>
			<td><%=card.getColor() %></td>
			<td><%=card.getRarity() %></td>
			<td><%=card.getReleaseLogo() %></td>
			<td><%=card.getManaCost() %></td>
			<td><%=card.getPower() %></td>
			<td><%=card.getToughness() %></td>
			<td><%=card.getHealth() %></td>
			<td><%=card.getQuantity() %></td>
		</tr>
		<%
			}
		%>
	</table>

</body>
</html>