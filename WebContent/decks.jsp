<%@page import="com.service.ServiceUtil"%>
<%@page import="com.entity.Deck"%>
<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Magic--Decks Management</title>
</head>
<body>
	<h1>Decks Management</h1>
	<table>
		<tr>
			<td style="width:100px"><a href = "index.jsp">Homepage</a></td>
			<td style="width: 100px"><a href="search.jsp?keyword=&indentoryId=<%=Integer.valueOf(session.getAttribute("inventoryid").toString())%>">Inventory</a></td>
			<td style="width:100px"><a href = "decks.jsp">Decks</a></td>
			<td style="width: 100px"><a href="addcards.jsp">Add Cards</a></td>
			<td style="width:100px"><a href = "login.jsp">Login</a></td>
		</tr>
	</table>
		<br>
	<%
		out.print("Captain: " + session.getAttribute("username"));
	%>
	<table border="1">


		<tr>
			<td>ID</td>
			<td>Name</td>
			<td>Delete</td>
			<td>Change</td>
			<td>List</td>
		</tr>

		<%
			List<Deck> decks = ServiceUtil.getDeckService().listByUserName((String) session.getAttribute("username"));
			for (int i = 0; i < decks.size(); i++) {
				Deck deck = decks.get(i);
		%>
		<tr>
			<td><%=deck.getId()%></td>
			<td><%=deck.getName()%></td>
			<td><a href="DeleteDeckAction?id=<%=deck.getId()%>">Delete</a></td>
			<td><form action="ModifyDeckAction" method="post">
					<label>Name:<input name="name" type="text" size="10"></label>
					<input name="id" value=<%=deck.getId()%> type="hidden">
					<button type="submit">Modify</button>
				</form></td>
			<td><a href="deckcards.jsp?id=<%=deck.getId()%>">List Cards</a></td>
		</tr>
		<%
			}
		%>
	</table>

	<form action="AddDeckAction" method="POST">
		<label>Deck Name <input name="name" type="text"></label>
		<button type="submit">Add</button>
	</form>

</body>
</html>