<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Magic</title>
</head>
<body>
<h1>Welcome!</h1>

	<table>
		<tr>
			<td style="width: 100px"><a href="index.jsp">Homepage</a></td>
			<td style="width: 100px"><a href="search.jsp?keyword=&indentoryId=<%=Integer.valueOf(session.getAttribute("inventoryid").toString())%>">Inventory</a></td>
			<td style="width: 100px"><a href="decks.jsp">Decks</a></td>
			<td style="width: 100px"><a href="addcards.jsp">Add Cards</a></td>
			<td style="width: 100px"><a href="login.jsp">Login</a></td>
		</tr>
	</table>
	<br>
	<% 
		out.print("Captain: "+session.getAttribute("username"));
	%>
</body>
</html>