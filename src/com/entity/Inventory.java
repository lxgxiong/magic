package com.entity;

import java.io.Serializable;
import java.util.Map;
import java.util.Set;

public class Inventory implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4403439291122449979L;

	private int id;
	private String username;
	private Set<Deck> decks;
	private Map<Card, Integer> cards; //this int is the quantity of the card.
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	
	public Set<Deck> getDecks() {
		return decks;
	}
	
	public void setDecks(Set<Deck> decks) {
		this.decks = decks;
	}
	
	public Map<Card, Integer> getCards() {
		return cards;
	}
	
	public void setCards(Map<Card, Integer> cards) {
		this.cards = cards;
	}
	
}
