package com.entity;

import java.io.Serializable;

public class Planeswalker extends Nonland implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -410262056465396379L;
	
	private int health;
	
	public int getHealth() {
		return health;
	}
	public void setHealth(int health) {
		this.health = health;
	}
	
}
