package com.entity;

import java.io.Serializable;
import java.util.Set;

public class User implements Serializable{
	
	private static final long serialVersionUID = 3512204083166994496L;
	
	private String username;
	private String password;
	private String email;
	private Set<Inventory> inventories;
	
	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
	
	public Set<Inventory> getInventories() {
		return inventories;
	}
	
	public void setInventories(Set<Inventory> inventories) {
		this.inventories = inventories;
	}
	
	
}
