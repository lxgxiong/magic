package com.entity;

import java.io.Serializable;

public class Creature extends Nonland implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 8932990504435560323L;
	
	private int power;
	private int toughness;
	
	public int getPower() {
		return power;
	}
	public void setPower(int power) {
		this.power = power;
	}
	public int getToughness() {
		return toughness;
	}
	public void setToughness(int toughness) {
		this.toughness = toughness;
	}
	
}
