package com.entity;

import java.io.Serializable;

public class Nonland extends Card implements Serializable{

	private static final long serialVersionUID = -4279664332022328336L;
	
	private String manaCost;
	
	public String getManaCost() {
		return manaCost;
	}
	public void setManaCost(String manaCost) {
		this.manaCost = manaCost;
	}

}
