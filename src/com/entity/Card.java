package com.entity;

import java.io.Serializable;

public class Card implements Serializable{
	
	private static final long serialVersionUID = -8055287841164843056L;
	
	private int id;
	private String name;
	private String color;
	private String rarity;
	private String description;
	private String releaseLogo;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getColor() {
		return color;
	}
	public void setColor(String color) {
		this.color = color;
	}
	public String getRarity() {
		return rarity;
	}
	public void setRarity(String rarity) {
		this.rarity = rarity;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getReleaseLogo() {
		return releaseLogo;
	}
	public void setReleaseLogo(String releaseLogo) {
		this.releaseLogo = releaseLogo;
	}
	
}
