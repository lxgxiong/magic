package com.entity;

import java.io.Serializable;
import java.util.Map;

public class Deck implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -6308726370673166860L;
	
	private int id;
	private String name;
	private String username;
	private Map<Card, Integer> cards;//this integer is the quantity of the card.
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	
	public Map<Card, Integer> getCards() {
		return cards;
	}
	
	public void setCards(Map<Card, Integer> cards) {
		this.cards = cards;
	}
	
}
