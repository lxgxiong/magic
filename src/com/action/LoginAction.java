package com.action;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.dto.UserDTO;
import com.service.ServiceUtil;

public class LoginAction extends HttpServlet {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -5972370850009291168L;

	@Override
	public void doPost(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		String username = req.getParameter("username");
		String password = req.getParameter("password");
	    try {
	    	UserDTO u = ServiceUtil.getUserService().login(username,password);
	    	if(u==null){
	    		PrintWriter out =  res.getWriter();
				out.println("You failed the world!");
	    	}else{
	    		HttpSession session = req.getSession();
				session.setAttribute("username", username);
				session.setAttribute("inventoryid", u.getInventoryid());
				res.setContentType("text/html; charset=UTF-8");
				res.sendRedirect("index.jsp");
	    	}
		} catch (Exception e) {
			e.printStackTrace();
		}
	    
	}

}
