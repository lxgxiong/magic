package com.action;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.dao.DaoUtil;
import com.entity.Deck;

public class ModifyDeckAction extends HttpServlet{

	/**
	 * 
	 */
	private static final long serialVersionUID = -2709439912148264823L;
	
	
	@Override
	protected void service(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		String name = req.getParameter("name");
		int id = Integer.valueOf(req.getParameter("id"));
		String username = req.getParameter("username");
		Deck deck = new Deck();
		deck.setId(id);
		deck.setName(name);
		deck.setUsername(username);
		DaoUtil.getDeckDao().modify(deck);
		res.sendRedirect("decks.jsp");
	}
}
