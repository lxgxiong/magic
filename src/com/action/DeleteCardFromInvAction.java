package com.action;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.service.ServiceUtil;

public class DeleteCardFromInvAction extends HttpServlet{

	/**
	 * 
	 */
	private static final long serialVersionUID = -5352664842469725094L;
	
	@Override
	protected void service(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		int inventoryId = Integer.valueOf(req.getParameter("id"));
		int quantity = Integer.valueOf(req.getParameter("quantity"));
		int cardId = Integer.valueOf(req.getParameter("cardid"));
		int hasQuantity = Integer.valueOf(req.getParameter("has"));
		if(hasQuantity<=quantity){
			ServiceUtil.getCardService().deleteCardFromInventory(inventoryId, cardId);
		}else{
			quantity=hasQuantity-quantity;
			ServiceUtil.getCardService().deleteCardFromInventory(inventoryId, cardId, quantity);
		}
		res.sendRedirect("inventory.jsp");
	}
}
