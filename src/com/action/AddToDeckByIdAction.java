package com.action;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.service.ServiceUtil;

public class AddToDeckByIdAction extends HttpServlet{

	/**
	 * 
	 */
	private static final long serialVersionUID = -5826604103015414948L;
	
	@Override
	protected void service(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		int deckId = Integer.valueOf(req.getParameter("deckId"));
		int cardId = Integer.valueOf(req.getParameter("cardId"));
		int quantity = Integer.valueOf(req.getParameter("quantity"));
		
		ServiceUtil.getCardService().addToDeckById(deckId, cardId, quantity);
	}

}
