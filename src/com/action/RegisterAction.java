package com.action;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.entity.User;
import com.service.ServiceUtil;

public class RegisterAction extends HttpServlet{
	/**
	 * 
	 */
	private static final long serialVersionUID = -1651553134450115380L;

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		User user = new User();
		user.setUsername(req.getParameter("username"));
		user.setPassword(req.getParameter("password"));
		user.setEmail(req.getParameter("email"));
		if(ServiceUtil.getUserService().register(user)){
			res.sendRedirect("login.jsp");
		}else{
			PrintWriter out =  res.getWriter();
			out.println("Shit happens, registration failed! Try again.");
		}
		
	}

}
