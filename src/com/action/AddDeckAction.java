package com.action;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.dao.DaoUtil;
import com.entity.Deck;

public class AddDeckAction extends HttpServlet{

	/**
	 * 
	 */
	private static final long serialVersionUID = 2415365131901946908L;
	
	@Override
	protected void service(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		HttpSession session = req.getSession();
		String name = req.getParameter("name");
		String username = (String) session.getAttribute("username");
		Deck deck = new Deck();
		deck.setName(name);
		deck.setUsername(username);
		DaoUtil.getDeckDao().add(deck);
		res.sendRedirect("decks.jsp");
	}

}
