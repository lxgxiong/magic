package com.action;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.simple.JSONObject;

import com.service.ServiceUtil;

public class SelectQuantityInDeck extends HttpServlet{

	/**
	 * 
	 */
	private static final long serialVersionUID = 2012884998515427297L;
	
	@Override
	protected void service(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		int cardId = Integer.valueOf(req.getParameter("cardId"));
		int deckId = Integer.valueOf(req.getParameter("deckId"));
		int quantity = ServiceUtil.getCardService().getQuantityInDeck(deckId, cardId);
		Map<String, Integer> result = new HashMap<String, Integer>();
		result.put("quantity", quantity);
		
		res.getWriter().println(JSONObject.toJSONString(result));
	}

}
