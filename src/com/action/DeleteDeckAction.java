package com.action;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.dao.DaoUtil;

public class DeleteDeckAction extends HttpServlet{
	/**
	 * 
	 */
	private static final long serialVersionUID = -2744373301860975704L;

	@Override
	protected void service(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		int id = Integer.valueOf(req.getParameter("id"));
		DaoUtil.getDeckDao().delete(id);
		res.sendRedirect("decks.jsp");
	}
}
