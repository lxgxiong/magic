package com.action;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.entity.Card;
import com.entity.Creature;
import com.entity.Nonland;
import com.entity.Planeswalker;
import com.service.ServiceUtil;

public class AddCardAction extends HttpServlet{

	/**
	 * 
	 */
	private static final long serialVersionUID = -5052754750507330390L;
	
	@Override
	protected void service(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		int type = Integer.valueOf(req.getParameter("type"));
		String name =  req.getParameter("name");
		String description = req.getParameter("description");
		String color = req.getParameter("color");
		String rarity =  req.getParameter("rarity");
		String releaseLogo = req.getParameter("releaselogo");
		int quantity = Integer.valueOf(req.getParameter("quantity"));
		int inventoryId = Integer.valueOf(req.getSession().getAttribute("inventoryid").toString());
		
		Card card = new Card();
		card.setName(name);
		card.setColor(color);
		card.setDescription(description);
		card.setRarity(rarity);
		card.setReleaseLogo(releaseLogo);
		if(type==1){
			ServiceUtil.getCardService().addLand(card, quantity,inventoryId);
		}else if(type==2){
			String manaCost = req.getParameter("manacost");

			int health = Integer.valueOf(req.getParameter("health"));

			Planeswalker planeswalker = new Planeswalker();
			planeswalker.setName(name);
			planeswalker.setColor(color);
			planeswalker.setDescription(description);
			planeswalker.setRarity(rarity);
			planeswalker.setReleaseLogo(releaseLogo);
			
			planeswalker.setHealth(health);
			planeswalker.setManaCost(manaCost);
			ServiceUtil.getCardService().addPlaneswalker(planeswalker, quantity,inventoryId);
		}else if(type==3){
			String manaCost = req.getParameter("manacost");

			int power = Integer.valueOf(req.getParameter("power"));
			int toughness = Integer.valueOf(req.getParameter("toughness"));
			
			Creature creature = new Creature();
			
			creature.setName(name);
			creature.setColor(color);
			creature.setDescription(description);
			creature.setRarity(rarity);
			creature.setReleaseLogo(releaseLogo);
			
			creature.setManaCost(manaCost);
			creature.setToughness(toughness);
			creature.setPower(power);
			ServiceUtil.getCardService().addCreature(creature, quantity,inventoryId);
		}else{
			String manaCost = req.getParameter("manacost");
			Nonland nonland = new Nonland();
			nonland.setName(name);
			nonland.setDescription(description);
			nonland.setRarity(rarity);
			nonland.setColor(color);
			nonland.setReleaseLogo(releaseLogo);
			nonland.setManaCost(manaCost);
			ServiceUtil.getCardService().addNonLand(nonland, quantity, inventoryId);
		}
		res.sendRedirect("addcards.jsp");
	}

}
