package com.dao;

public class DaoUtil {
	private static CardsDao cardsDao;
	private static InventoryDao inventoryDao;
	private static UserDao userDao;
	private static DeckDao deckDao;
	
	
	public static CardsDao getCardsDao() {
		if(cardsDao==null){
			synchronized (CardsDao.class) {
				cardsDao=new CardsDao();
			}
		}
		return cardsDao;
	}
	
	public static InventoryDao getInventoryDao() {
		if(inventoryDao==null){
			synchronized (InventoryDao.class) {
				inventoryDao=new InventoryDao();
			}
		}
		return inventoryDao;
	}
	
	public static UserDao getUserDao() {
		if(userDao==null){
			synchronized (InventoryDao.class) {
				userDao=new UserDao();
			}
		}
		return userDao;
	}
	
	public static DeckDao getDeckDao() {
		if(deckDao==null){
			synchronized (DeckDao.class) {
				deckDao=new DeckDao();
			}
		}
		return deckDao;
	}
}
