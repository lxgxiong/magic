package com.dao;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

import org.apache.commons.dbutils.DbUtils;
import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.BeanListHandler;

import com.entity.Inventory;
import com.utils.DBHelper;

public class InventoryDao {

	public List<Inventory> list(String username) {
		Connection con = DBHelper.getConnection();
		List<Inventory> inventories = null;
		QueryRunner qr = new QueryRunner();
		String sql = "SELECT * FROM inventory WHERE username = ?";
		try {
			inventories = qr.query(con, sql, new BeanListHandler<>(Inventory.class), username);
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				DbUtils.close(con);
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return inventories;
	}

	public void deleteInventoryById(int id) {
		Connection con = DBHelper.getConnection();
		QueryRunner qr = new QueryRunner();
		String sql = "DELETE FROM inventory WHERE id=?";
		try {
			qr.update(con, sql, id);
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				DbUtils.close(con);
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

	public void insertInventory(Inventory inventory) {

	}

}
