package com.dao;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

import org.apache.commons.dbutils.DbUtils;
import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.BeanListHandler;

import com.entity.Deck;
import com.utils.DBHelper;

public class DeckDao {
	public void add(Deck deck) {
		Connection conn = DBHelper.getConnection();
		QueryRunner qr = new QueryRunner();
		String sql = "INSERT INTO deck (username, name) VALUES(?,?) ";
		try {
			qr.update(conn, sql, deck.getUsername(), deck.getName());
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				DbUtils.close(conn);
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

	public void delete(int deckId) {

		DaoUtil.getCardsDao().removeAllFromDeck(deckId);
		Connection conn = DBHelper.getConnection();
		QueryRunner qr = new QueryRunner();
		String sql = "DELETE FROM deck WHERE id=?";
		try {
			qr.update(conn, sql, deckId);
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				DbUtils.close(conn);
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

	public void modify(Deck deck) {

		Connection conn = DBHelper.getConnection();
		QueryRunner qr = new QueryRunner();
		String sql = "UPDATE deck set name=? WHERE id=" + deck.getId();
		try {
			qr.update(conn, sql, deck.getName());
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				DbUtils.close(conn);
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

	public List<Deck> listByUserName(String username) {
		Connection con = DBHelper.getConnection();
		List<Deck> decks = null;
		QueryRunner qr = new QueryRunner();
		String sql = "SELECT * FROM deck WHERE username = ?";
		try {
			decks = qr.query(con, sql, new BeanListHandler<>(Deck.class), username);
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				DbUtils.close(con);
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return decks;
	}

}
