package com.dao;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

import org.apache.commons.dbutils.DbUtils;
import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.BeanHandler;
import org.apache.commons.dbutils.handlers.BeanListHandler;
import org.apache.commons.dbutils.handlers.ScalarHandler;

import com.dto.CardDTO;
import com.dto.DContainsCardDTO;
import com.entity.Card;
import com.entity.Creature;
import com.entity.Nonland;
import com.entity.Planeswalker;
import com.utils.DBHelper;

public class CardsDao {
	public void modifyCardById(int id) {
		
	}

	public void addLand(Card card, int quantity, int inventoryId) {
		Connection conn = DBHelper.getConnection();
		QueryRunner qr = new QueryRunner();
		String sql = "INSERT INTO card (name, color, rarity, description,releaselogo, island) VALUES (?, ?, ?, ?,?,?)";
		String sql1 = "INSERT INTO icontainscard (quantity, CARD_id, INVENTORY_id) VALUES (?, ?, ?)";
		try {
			long id = qr.insert(conn, sql, new ScalarHandler<>(), card.getName(), card.getColor(), card.getRarity(),
					card.getDescription(), card.getReleaseLogo(), true);
			qr.insert(conn, sql1,new ScalarHandler<>(),quantity, id,inventoryId);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public void addNonLand(Nonland nonland, int quantity, int inventoryId){
		Connection conn = DBHelper.getConnection();
		QueryRunner qr = new QueryRunner();
		String sql = "INSERT INTO card (name, color, rarity, description,releaselogo, island) VALUES (?, ?, ?, ?,?,?)";
		try {
			long id = qr.insert(conn,sql,new ScalarHandler<>(),nonland.getName(),nonland.getColor(),nonland.getRarity(),nonland.getDescription(),nonland.getReleaseLogo(),false);
			String sql1 = "INSERT INTO icontainscard (quantity, CARD_id, INVENTORY_id) VALUES (?, ?, ?)";
			qr.insert(conn, sql1,new ScalarHandler<>(),quantity, id,inventoryId);
			String sql2 = "INSERT INTO nonland (manacost,isplaneswalker,iscreature,CARD_id) VALUES(?,?,?,?)";
			qr.insert(conn, sql2,new ScalarHandler<>(),nonland.getManaCost(),false,false,id);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public void addPlaneswalker(Planeswalker planeswalker, int quantity, int inventoryId) {
		Connection conn = DBHelper.getConnection();
		QueryRunner qr = new QueryRunner();
		String sql = "INSERT INTO card (name, color, rarity, description,releaselogo, island) VALUES (?, ?, ?, ?,?,?)";
		try {
			long id = qr.insert(conn, sql,new ScalarHandler<>(), planeswalker.getName(),planeswalker.getColor(),planeswalker.getRarity(),planeswalker.getDescription(),planeswalker.getReleaseLogo(),false);
			String sql1 = "INSERT INTO icontainscard (quantity, CARD_id, INVENTORY_id) VALUES (?, ?, ?)";
			qr.insert(conn, sql1,new ScalarHandler<>(),quantity, id,inventoryId);
			String sql2 = "INSERT INTO nonland (manacost,isplaneswalker,iscreature,CARD_id) VALUES(?,?,?,?)";
			long nonlandID = qr.insert(conn, sql2,new ScalarHandler<>(),planeswalker.getManaCost(),true,false,id);
			String sql3 = "INSERT INTO planeswaler (health,NONLAND_id) VALUES (?,?)";
			qr.insert(conn, sql3,new ScalarHandler<>(),planeswalker.getHealth(),nonlandID);
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public void addCreature(Creature creature, int quantity, int inventoryId) {
		Connection conn = DBHelper.getConnection();
		QueryRunner qr = new QueryRunner();
		String sql = "INSERT INTO card (name, color, rarity, description,releaselogo, island) VALUES (?, ?, ?, ?,?,?)";
		try {
			long id = qr.insert(conn, sql,new ScalarHandler<>(), creature.getName(),creature.getColor(),creature.getRarity(),creature.getDescription(),creature.getReleaseLogo(),false);
			String sql1 = "INSERT INTO icontainscard (quantity, CARD_id, INVENTORY_id) VALUES (?, ?, ?)";
			qr.insert(conn, sql1,new ScalarHandler<>(),quantity, id,inventoryId);
			String sql2 = "INSERT INTO nonland (manacost,isplaneswalker,iscreature,CARD_id) VALUES(?,?,?,?)";
			long nonlandID = qr.insert(conn, sql2,new ScalarHandler<>(),creature.getManaCost(),false,true,id);
			String sql3 = "INSERT INTO creature (power,toughness,NONLAND_id) VALUES (?,?,?)";
			qr.insert(conn, sql3,new ScalarHandler<>(),creature.getPower(),creature.getToughness(),nonlandID);
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public List<CardDTO> listByInventoryId(int id) {
		List<CardDTO> cards = null;
		Connection conn = DBHelper.getConnection();
		QueryRunner qr = new QueryRunner();
		String sql = "select c.id,c.name,c.color,c.rarity,c.description, c.releaselogo,c.island,d.quantity from card c left join icontainscard d on c.id=d.CARD_id where INVENTORY_id=?";
		try {
			cards = qr.query(conn, sql, new BeanListHandler<>(CardDTO.class), id);
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				DbUtils.close(conn);
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return cards;
	}

	public List<CardDTO> listByDeckId(int id) {
		List<CardDTO> cards = null;
		Connection conn = DBHelper.getConnection();
		QueryRunner qr = new QueryRunner();
		String sql = "select c.id,c.name,c.color,c.rarity,c.description, c.releaselogo,c.island,d.quantity from card c left join dcontainscard d on c.id=d.CARD_id where DECK_id=?";
		try {
			cards = qr.query(conn, sql, new BeanListHandler<>(CardDTO.class), id);
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				DbUtils.close(conn);
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return cards;
	}

	public void removeAllFromDeck(int deckId) {
		Connection conn = DBHelper.getConnection();
		QueryRunner qr = new QueryRunner();
		String sql = "DELETE from dcontainscard WHERE DECK_id=?";
		try {
			qr.update(conn, sql, deckId);
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				DbUtils.close(conn);
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

	public void removeFromInventory(int inventoryId, int cardId) {
		Connection conn = DBHelper.getConnection();
		QueryRunner qr = new QueryRunner();
		String sql = "DELETE from icontainscard WHERE INVENTORY_id=? AND CARD_id=?";
		String sql1 = "DELETE from dcontainscard WHERE CARD_id=?";
		try {
			qr.update(conn, sql, inventoryId, cardId);
			qr.update(conn, sql1, cardId);
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				DbUtils.close(conn);
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

	public void removeFromInventory(int inventoryId, int cardId, int quantity) {
		Connection conn = DBHelper.getConnection();
		QueryRunner qr = new QueryRunner();
		String sql = "UPDATE icontainscard SET quantity=? WHERE INVENTORY_id=? AND CARD_id=?";
		try {
			qr.update(conn, sql, quantity, inventoryId, cardId);
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				DbUtils.close(conn);
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

	public void removeFromDeck(int cardId, int deckId) {

	}

	public int getQuantityInDeck(int cardId, int deckId) {
		Connection conn = DBHelper.getConnection();
		QueryRunner qr = new QueryRunner();
		String check = "SELECT * FROM dcontainscard WHERE DECK_id=? AND CARD_id=?";
		try {
			DContainsCardDTO d = qr.query(conn, check, new BeanHandler<>(DContainsCardDTO.class), deckId, cardId);
			if (d == null) {
				return 0;
			} else {
				return d.getQuantity();
			}
		} catch (SQLException e) {
			return 0;
		} finally {
			try {
				DbUtils.close(conn);
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

	public void addToDeckById(int cardId, int deckId, int quantity) {
		Connection conn = DBHelper.getConnection();
		QueryRunner qr = new QueryRunner();
		String check = "SELECT * FROM dcontainscard WHERE DECK_id=? AND CARD_id=?";
		try {
			DContainsCardDTO d = qr.query(conn, check, new BeanHandler<>(DContainsCardDTO.class), deckId, cardId);
			if (d == null) {
				String sql = "INSERT INTO dcontainscard (CARD_id, DECK_id,quantity) VALUES(?,?,?)";
				qr.update(conn, sql, cardId, deckId, quantity);
			} else {
				quantity += d.getQuantity();
				String sql = "UPDATE dcontainscard SET quantity=? WHERE id=?";
				qr.update(conn, sql, quantity, d.getId());
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				DbUtils.close(conn);
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

	public List<Card> listAllCards() {
		return null;
	}

	public List<Card> searchInAll(String str) {
		return null;
	}

	public List<CardDTO> searchInInventory(String str, int inventoryId) {
		Connection conn = DBHelper.getConnection();
		QueryRunner qr = new QueryRunner();
		String sql = "SELECT c.id,c.name,c.color,c.rarity,c.description,c.releaselogo,n.manacost,p.health,cr.power,cr.toughness,i.quantity FROM card c left join nonland n on c.id=n.CARD_id left join planeswalker p on n.id=p.NONLAND_id left join creature cr on cr.NONLAND_id=n.id left join icontainscard i on i.CARD_id=c.id where (c.releaselogo like ? or c.name like ? or c.description like ? or c.rarity like ?) and i.INVENTORY_id=?";
		String keyword = "%"+str+"%";
		List<CardDTO> cards =null;
		try {
			cards = qr.query(conn,sql,new BeanListHandler<>(CardDTO.class),keyword,keyword,keyword,keyword,inventoryId);
		} catch (SQLException e) {
			e.printStackTrace();
		}finally {
			try {
				DbUtils.close(conn);
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return cards;
	}

	public List<Card> searchInDeck(String str, int deckId) {
		return null;
	}
}
