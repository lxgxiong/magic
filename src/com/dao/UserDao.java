package com.dao;

import java.sql.Connection;
import java.sql.SQLException;

import org.apache.commons.dbutils.DbUtils;
import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.BeanHandler;

import com.dto.UserDTO;
import com.entity.User;
import com.utils.DBHelper;

public class UserDao {

	public UserDTO login(String username, String password) {
		Connection con = DBHelper.getConnection();
		QueryRunner qr = new QueryRunner();
		String sql = "select u.username,u.email, i.id as inventoryid from user u left join inventory i on u.username=i.username where u.username=? and u.password=?";
		UserDTO u = null;
		try {
			u = qr.query(con, sql, new BeanHandler<>(UserDTO.class), username, password);
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				DbUtils.close(con);
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return u;
	}

	public boolean register(User user) {
		Connection con = DBHelper.getConnection();
		QueryRunner qr = new QueryRunner();
		String sql = "INSERT INTO user (username, password,email) VALUES(?,?,?) ";
		try {
			qr.update(con, sql, user.getUsername(), user.getPassword(), user.getEmail());
			qr.update(con, "INSERT INTO inventory (username) VALUES(?)", user.getUsername());
			return true;
		} catch (SQLException e1) {
			return false;
		} finally {
			try {
				DbUtils.close(con);
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}
}
