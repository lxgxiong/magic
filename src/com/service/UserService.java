package com.service;

import com.dao.DaoUtil;
import com.dto.UserDTO;
import com.entity.User;

public class UserService {
	
	public UserDTO login(String username,String password) throws Exception{
		return DaoUtil.getUserDao().login(username, password);
	}
	
	public boolean register(User user) {
		return DaoUtil.getUserDao().register(user);
	}
}
