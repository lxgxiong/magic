package com.service;

import java.util.List;

import com.dao.DaoUtil;
import com.entity.Deck;

public class DeckService {
	public List<Deck> listByUserName(String username) {
		return DaoUtil.getDeckDao().listByUserName(username);
	}
}
