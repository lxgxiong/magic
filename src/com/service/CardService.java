package com.service;

import java.util.List;

import com.dao.DaoUtil;
import com.dto.CardDTO;
import com.entity.Card;
import com.entity.Creature;
import com.entity.Nonland;
import com.entity.Planeswalker;

public class CardService {
	public List<CardDTO> listByDeckId(int id) {
		List<CardDTO> cards =  DaoUtil.getCardsDao().listByDeckId(id);
		return cards;
	}
	
	public List<CardDTO> listByInventoryId(int id) {
		List<CardDTO> cards =  DaoUtil.getCardsDao().listByInventoryId(id);
		return cards;
	}
	
	public void deleteCardFromInventory(int inventoryId,int cardId, int quantity) {
		DaoUtil.getCardsDao().removeFromInventory(inventoryId, cardId, quantity);
	}
	
	public void deleteCardFromInventory(int inventoryId,int cardId) {
		DaoUtil.getCardsDao().removeFromInventory(inventoryId, cardId);
	}
	
	public void addToDeckById(int deckId,int cardId, int quantity){
		DaoUtil.getCardsDao().addToDeckById(cardId, deckId, quantity);
	}
	
	public int getQuantityInDeck(int deckId, int cardId){
		return DaoUtil.getCardsDao().getQuantityInDeck(cardId, deckId);
	}
	
	public void addLand(Card card, int quantity,int inventoryId){
		DaoUtil.getCardsDao().addLand(card, quantity,inventoryId);
	}
	
	public void addNonLand(Nonland nonland, int quantity, int inventoryId){
		DaoUtil.getCardsDao().addNonLand(nonland, quantity, inventoryId);
	}
	
	public void addPlaneswalker(Planeswalker planeswalker, int quantity,int inventoryId) {
		DaoUtil.getCardsDao().addPlaneswalker(planeswalker, quantity,inventoryId);
	}
	
	public void addCreature(Creature creature, int quantity, int inventoryId) {
		DaoUtil.getCardsDao().addCreature(creature, quantity,inventoryId);
	}
	
	public List<CardDTO> searchInInventory(String keyword, int inventoryId) {
		return DaoUtil.getCardsDao().searchInInventory(keyword, inventoryId);
	}
}
