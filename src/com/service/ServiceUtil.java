package com.service;

public class ServiceUtil {
	private static UserService userService;
 	private static DeckService deckService;
	private static CardService cardService;
	
	public static UserService getUserService() {
		if(userService==null){
			synchronized (UserService.class) {
				userService=new UserService();
			}
		}
		return userService;
	}
	
 	public static DeckService getDeckService() {
 		if(deckService==null){
 			synchronized (DeckService.class) {
 				deckService=new DeckService();
 			}
 		}
 		return deckService;
 	}
	
	public static CardService getCardService() {
		if(cardService==null){
			synchronized (CardService.class) {
				cardService=new CardService();
			}
		}
		return cardService;
	}
}
