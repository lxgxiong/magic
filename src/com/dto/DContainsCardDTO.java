package com.dto;

import java.io.Serializable;

public class DContainsCardDTO implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -6773512009752441010L;
	
	private int id;
	private int CARD_id;
	private int DECK_id;
	private int quantity;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getCARD_id() {
		return CARD_id;
	}
	public void setCARD_id(int cARD_id) {
		CARD_id = cARD_id;
	}
	public int getDECK_id() {
		return DECK_id;
	}
	public void setDECK_id(int dECK_id) {
		DECK_id = dECK_id;
	}
	public int getQuantity() {
		return quantity;
	}
	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
	
	

}
