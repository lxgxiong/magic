package com.utils;

import java.sql.Connection;
import java.sql.DriverManager;


public class DBHelper {
	
	private static DBHelper instance = null;  
    private static Connection conn = null;  
    private static String Driver = ConfigLoaderUtils.getProperty("jdbc.driver");  
    private static String Url = ConfigLoaderUtils.getProperty("jdbc.url");  
    private static String UserName = ConfigLoaderUtils.getProperty("jdbc.userName");  
    private static String PassWord = ConfigLoaderUtils.getProperty("jdbc.passWord");  
      
    public static DBHelper getInstance() {  
        if (instance == null) {  
            synchronized (DBHelper.class) {  
                instance = new DBHelper();  
            }  
        }  
        return instance;  
    }  

    public static Connection getConnection() {
        try {  
            Class.forName(Driver);  
            conn = DriverManager.getConnection(Url, UserName, PassWord);  
        } catch (Exception e) {  
            e.printStackTrace();  
        }  
        return conn;  
    }  
  
}
