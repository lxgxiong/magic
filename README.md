Set up project:


1. Download IDE, https://www.eclipse.org/downloads/ , download the Eclipse IDE for java EE developers. (Assume you have JDK installed)
2. Download Tomcat. http://tomcat.apache.org/download-80.cgi
3. Fork the repo
4. Import Dump20151117.sql in this repo to your Local MySQL. 
5. Open Eclipse, new a "Dynamic Web Project" (name as "magic")
6. Remove the existed "src" and "WebContent" in the project
7. Import "src" as scource folder from the repo to your project
8. Import "WebContent" to your project
9. Right click the project folder "magic" in eclipse, then "properties", then "Targeted Runtimes", then new a tomcat 8

How to run? (Before running, modify src/prop/jdbc.properties file, replace the username and password to your own local mysql username and password)

1. right click on the project folder magic, "run as" -> "Run on server" 
2. your eclipse will open a webpage in its editor (or you can use your own Browser type in: http://localhost:8080/magic/)

To-Do list: (UserDao is an example for your information to implementing below methods)

1. Implement the unimplemented methods in CardsDao
2. Implement the unimplemented methods in InventoryDao
3. To be continued...

Feel free to email me if you have any questions..

Lei